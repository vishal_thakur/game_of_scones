﻿using UnityEngine;
using System.Collections;

public class MenuNavigation : MonoBehaviour {

	public void MainMenu()
	{
		Application.LoadLevel("menu");
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void PlayNormalMode()
	{
		//Set Game mode as Normal
		Globals.CurrentGameMode = Globals.GameDifficulty.Normal;
		Application.LoadLevel("game");
	}

	public void PlayAdvancedMode()
	{
		//Set Game mode as Advanced
		Globals.CurrentGameMode = Globals.GameDifficulty.Advanced;
		Application.LoadLevel("game");
	}

	public void HighScores()
	{
		Application.LoadLevel("scores");
		
	}

    public void Credits()
    {
        Application.LoadLevel("credits");
    }

	public void SourceCode()
	{
		Application.OpenURL("https://github.com/vilbeyli/Pacman-Clone/");
	}
}
