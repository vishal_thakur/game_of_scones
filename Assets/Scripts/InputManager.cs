﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    #region Params
    public enum Direction
    {
        None = 0,
        Up,
        Down,
        Left,
        Right
    }

    List<Vector2> Compass = new List<Vector2>();


    [SerializeField]
    Joystick JoystickRef;


    //Represents the Current Direction of the Fixed Joystick
    public Direction CurrentJoystickDirection;

    //Represents the Current Direction of the Fixed Joystick
    //public Direction PreviousJoystickDirection;

    //Represents the Current Direction of the Fixed Joystick
    public Vector2 CurrentJoystickDirectionVector2;

    //Represents the Current Direction of the Fixed Joystick
   // public Vector2 PreviousJoystickDirectionVector2;

    #endregion

    private void Awake()
    {
        AddDirectionsToCompass();
    }




    //private void OnGUI()
    //{
    //    GUILayout.TextField("Vertical = " + fixedJoystickRef.Direction.normalized.y);
    //    GUILayout.TextField("Horizontal = " + fixedJoystickRef.Direction.normalized.x);
    //    GUILayout.TextField("Direction = " + fixedJoystickRef.Direction);
    //    GUILayout.TextField("Compass Direction = " + GetJoystickCompassDirection());
    //    //GUILayout.TextField
    //}

     void Update()
    {
        //Grab the Current Joystick Direction every Frame
        CurrentJoystickDirection = GetJoystickCompassDirection();
    }



    #region Utility

    //Adds All Possible directions to the Compass
    private void AddDirectionsToCompass()
    {
        Compass.Add(Vector2.zero);
        Compass.Add(Vector2.up);
        Compass.Add(Vector2.down);
        Compass.Add(Vector2.left);
        Compass.Add(Vector2.right);
    }


    //Gets the Current Joystick's Direction
    Direction GetJoystickCompassDirection()
    {

        var maxDot = -Mathf.Infinity;
        var ret = Vector2.zero;
        int directionIndex = 0;

        for (int i = 0; i < Compass.Count; i++)
        {
            var t = Vector3.Dot(JoystickRef.Direction, Compass[i]);
            if (t > maxDot)
            {
                ret = Compass[i];
                CurrentJoystickDirectionVector2 = ret;
                maxDot = t;
                directionIndex = i;
            }
        }

        return (Direction)directionIndex;

    }
    #endregion
}
