﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
    public enum GameDifficulty
    {
        Normal,
        Advanced
    }

    public class GameMode {
        public GameMode(GameDifficulty difficulty, float pacmanspeed , float enemySpeed)
        {
            GameDifficulty = difficulty;
            PacmanSpeed = pacmanspeed;
            EnemySpeed = enemySpeed;
        }
        //Represents the Current Game Difficulty
        public GameDifficulty GameDifficulty;
        public float PacmanSpeed;
        public float EnemySpeed;
    }

    //Represents the Current Game mode according to the Difficulty
    public static GameDifficulty CurrentGameMode = GameDifficulty.Normal;

    //Possible Game Modes and their Params (GameDifficulty , PacmanSpeed , EnemySpeed)
    public static GameMode NormalGameModeParams = new GameMode(GameDifficulty.Normal, 0.13f, 0.08f);
    public static GameMode AdvancedGameModeParams = new GameMode(GameDifficulty.Normal, 0.2f, 0.15f);

}
